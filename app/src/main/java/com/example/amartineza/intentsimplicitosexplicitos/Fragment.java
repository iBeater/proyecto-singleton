package com.example.amartineza.intentsimplicitosexplicitos;

import android.os.Bundle;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by amartineza on 2/28/2018.
 */

public class Fragment extends android.app.Fragment {
    public Fragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment, container, false);
        Button botonFragment = (Button)view.findViewById(R.id.boton_fragment);
        final TextView textFragment = (TextView)view.findViewById(R.id.text_fragment);

        botonFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textFragment.setText(Singleton.getInstance().msg);
            }
        });

        return view;

    }

}
