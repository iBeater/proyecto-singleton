package com.example.amartineza.intentsimplicitosexplicitos;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.File;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = (Button) findViewById(R.id.boton);
        Button buttonFragment = (Button)findViewById(R.id.boton_go_to_fragment);

        Modelo modelo = new Modelo("Nombre Mascota: Marce", "Nombre Dueño: Adriana",
                "Sexo Mascota: Hembra", "Tipo Animal: Gato");
        Gson gson = new Gson();
        final String json = gson.toJson(modelo);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, BActivity.class);
                intent.putExtra(BActivity.EXTRA_KEY, json);
                startActivity(intent);
            }
        });

        buttonFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, FragmentActivity.class);
                startActivity(intent);
            }
        });
    }
}

