package com.example.amartineza.intentsimplicitosexplicitos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.google.gson.Gson;

public class BActivity extends AppCompatActivity {

    public static final String EXTRA_KEY = "key";
    TextView tvNombreDueño;
    TextView tvNombreMascota;
    TextView tvSexoMascota;
    TextView tvTipoMascota;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b);

        tvNombreDueño = (TextView)findViewById(R.id.nombre_dueño);
        tvNombreMascota = (TextView)findViewById(R.id.nombre_mascota);
        tvSexoMascota = (TextView)findViewById(R.id.sexo_mascota);
        tvTipoMascota = (TextView)findViewById(R.id.tipo_mascota);

        if(getIntent().getExtras()!=null){
            Gson gson = new Gson();
            Modelo model =  gson.fromJson(getIntent().getExtras().getString(EXTRA_KEY), Modelo.class);
           //tvNombre.setText(getIntent().getExtras().getString(EXTRA_KEY));

            tvNombreDueño.setText(model.getNombreDueño());
            tvNombreMascota.setText(model.getNombreMascota());
            tvSexoMascota.setText(model.getSexoMascota());
            tvTipoMascota.setText(model.getTipoMascota());

        }


    }
}
