package com.example.amartineza.intentsimplicitosexplicitos;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class FragmentActivity extends AppCompatActivity {

    EditText etTextoFragmentActivity;
    String textoActivity;

   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

       etTextoFragmentActivity = (EditText)findViewById(R.id.et_activity_fragment);
        Button boton = (Button)findViewById(R.id.button_Activity_Fragment);

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               textoActivity = etTextoFragmentActivity.getText().toString();
               Singleton.getInstance().msg=textoActivity;
            }
        });


        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment, new Fragment());
        fragmentTransaction.commit();



    }
}
